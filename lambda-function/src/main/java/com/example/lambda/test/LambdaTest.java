package com.example.lambda.test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LambdaTest {

    public static final String aaaaa = "bbb";

    public static void main(String[] args) {

        LambdaTest lambdaTest = new LambdaTest();
        lambdaTest.runnable();
        lambdaTest.iterTest();
        lambdaTest.mapTest();
    }

    public void runnable(){
        new Thread(()-> System.out.println("It's a lambda function!")).start();
    }

    public void iterTest(){
        List<String> languages = Arrays.asList("java","scala","python");
        //before java8
        for(String each:languages) {
            System.out.println(each);
        }
        //after java8
        languages.forEach(x -> System.out.println(x));
        languages.forEach(System.out::println);
        languages.forEach(x -> {
            System.out.println(x);
        });
    }

    public void mapTest(){
        List<Double> cost = Arrays.asList(10.0, 20.0,30.0,40.0);
        cost.stream().map(x -> x+x*0.05).forEach(System.out::println);
        Double aDouble = cost.stream().map(x -> x + x * 0.05).reduce((sum, x) -> sum + x).get();
        System.out.println(aDouble);
        List<Double> collect = cost.stream().filter((x) -> x > 25).map(x -> x+10).sorted().collect(Collectors.toList());
        System.out.println(collect);
    }

    public void functionTest(){
        int a = 0;
        String sb = "";
        if("aaa".equals(sb)){

        }
        Executors.newCachedThreadPool();
    }

}
