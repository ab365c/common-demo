package com.example.webflux.controller;

import org.junit.Test;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 1. 实例方法引用object::methodName
 * 2. 构造方法引用className::new
 * 3. 基于参数实例方法引用className::methodName
 * 4. 静态方法应用className::staticMethodName
 */
public class LambdaTest {
    public String method(String s) {
        System.out.println(s);
        return s + "dddd";
    }

    public interface Compare {
        int do(String n, String v);
    }

    public interface TestGet {
        String get();
    }

    public interface TestCC {
        String do(LambdaTest n, String v);
    }

    @Test
    public void one() {
        // 实例方法引用
        LambdaTest cc = new LambdaTest();
        Function<String, String> d = cc::method;
        System.out.println(d.apply("ddd"));

        System.out.println(((Function<String, String>) cc::method).apply("ccc"));

        // 基于参数实例方法引用
        Compare c = String::compareTo;
        System.out.println(c.do("1", "2"));

        // 基于参数实例方法引用(根据参数类型自动推导）
        TestCC sdf = LambdaTest::method;
        sdf.do(new LambdaTest(), "test::method");

        // 构造方法引用
        TestGet f = String::new;
        System.out.println(f.get());

        // Supplier
        Supplier<String> t = String::new;
        System.out.println(t.get());

        // 静态引用
        Function<String, Integer> dd = Integer::valueOf;
        System.out.println(dd.apply("3"));

        System.out.println(((Function<String, Integer>) Integer::valueOf).apply("3"));
    }
}