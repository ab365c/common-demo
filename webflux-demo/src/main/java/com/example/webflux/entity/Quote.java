package com.example.webflux.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Instant;

/**
 * @description:
 * @author: ADMIN
 * @createTime: 2020/3/18 16:51
 * @version:1.0
 */
@Data
public class Quote {
    private static final MathContext MATH_CONTEXT = new MathContext(2);

    private String ticker;

    private BigDecimal price;

    private Instant instant;

    public Quote(){}

    public Quote(String ticker, BigDecimal price){
        this.ticker = ticker;
        this.price = price;
    }

    public Quote(String ticker, Double price){
        // 可能会造成小数精度丢失
        this(ticker, new BigDecimal(price));
    }

    @Override
    public String toString() {
        return "Quote{" +
                "ticker='" + ticker + '\'' +
                ", price=" + price +
                ", instant=" + instant +
                '}';
    }
}
