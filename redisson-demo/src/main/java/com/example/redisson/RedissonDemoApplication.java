package com.example.redisson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @author: ADMIN
 * @createTime: 2020/3/18 17:23
 * @version:1.0
 */
@SpringBootApplication
public class RedissonDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedissonDemoApplication.class, args);
    }
}
